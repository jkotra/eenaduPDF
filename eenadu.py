import os
import logging
import argparse
from utils import Editions, download_and_merge
from parser import eenaduParser
from telegram_post import TelegramPoster

# set logging
if os.getenv("EENADU_DEBUG") is not None:
    logging.basicConfig(level=os.getenv("EENADU_DEBUG"), format='%(levelname)s:%(name)s.py:%(funcName)s:%(message)s')

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(description="Eenadu(ఈనాడు) ePaper Downloader")
parser.add_argument('-e', '--edition',  type=str, default = "HYDERABAD", 
                    choices = ["ANDHRAPRADESH", "TELANGANA", "HYDERABAD", "SUNDAY"])          
parser.add_argument('-d', '--date', required=False,  type=str, help="date of e-paper (DD/MM/YYYY)")
parser.add_argument('-t', '--telegram', nargs='?', type=str, required=False, const="telegram.json", help="Telegram JSON Config")
parser.add_argument('-c', '--cookies', required=False,  type=str, default="cookies.txt", help="Cookie Dump.")
parser.add_argument('-q', '--quality', required=False,  type=int, default=70, help="Quality factor of JPEG [1-100]")
parser.add_argument('-p', '--poster', required=False,  action="store_true", default=False, help="Send Poster to Telegram")
parser.add_argument('--head', required=False,  action="store_true", default=False, help="Run FireFox without --headless option.")
args = parser.parse_args()

def main():
    logger.debug(args)
    parser = eenaduParser(args.cookies, args.date, args.head)
    parser.get_edition(Editions[args.edition.upper()])
    
    poster = download_and_merge(parser.gen_filename(), parser.get_links(), args.quality, poster=args.poster)
    logger.info(poster)

    if args.telegram is not None:
        t = TelegramPoster(args.telegram)

        if args.poster:
            t.send_poster(poster)
            
        t.post(parser.gen_filename())


if __name__ == "__main__":
    main()