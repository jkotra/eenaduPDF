import json
import os

from telegram import Bot
from telegram.parsemode import ParseMode
import logging

import yadisk
from yandex_disk_utils import upload

logger = logging.getLogger(__name__)

class TelegramPoster:

    def __init__(self, config: str):
        self.t_config = json.loads(open(config).read())
        self.chan_id = self.t_config["chan_id"]
        self.bot_token = self.t_config["bot_token"]

        self.bot = Bot(self.bot_token)

    def post(self, file: str):
        if (size := os.stat(file).st_size) >= 50_000_000:
            y = yadisk.YaDisk(token=self.t_config["ya_token"])
            link = upload(y, file)
            message_body = f"<b>{file}</b>\n\n\n<a href=\"{link}\">{'🛸 Yandex.Disk'}</a>\n"
            logger.debug(message_body)
            self.bot.send_message(self.chan_id, message_body, parse_mode=ParseMode.HTML)
        else:
            self.bot.send_document(self.chan_id,
                                open(file, "rb").read(),
                                filename=file)
    
    def send_poster(self, image: str):
        self.bot.send_photo(self.chan_id, open(image, "rb"))