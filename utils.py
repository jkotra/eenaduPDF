import logging
import requests
import pyvips
import img2pdf
from pathlib import Path
import os
from typing import List, Tuple
import enum

logger = logging.getLogger(__name__)

class Editions(enum.Enum):
    TELANGANA = 1
    ANDHRAPRADESH = 2
    HYDERABAD = 3
    SUNDAY = 4

def download_and_merge(filename: str, data: List[Tuple[str, str]], quality: int, poster: bool = False) -> str or None:
    processed = []

    file = Path(filename)

    if file.exists():
        os.remove(file)
        logger.warning("pre-existing file %s removed!", file)

    for (image_layer, text_layer) in data:
        logger.info("\n\tpair =>\n\timage layer = %s \n\t(x)\n\ttext layer = %s", image_layer, text_layer)
        image = requests.get(image_layer)
        text = requests.get(text_layer)

        image = pyvips.Image.new_from_buffer(image.content, "", access="sequential").addalpha()
        text = pyvips.Image.new_from_buffer(text.content, "", access="sequential")

        image = image.composite2(text, pyvips.enums.BlendMode.ATOP)
        if quality == 100:
            image = image.write_to_buffer(".png")
        else:
            image = image.write_to_buffer(".jpg", Q=quality)
        processed.append(image)
        logger.debug("pair appended!")
    
    with open(file, "wb+") as pdf:
        pdf.write(img2pdf.convert(processed))
    
    logger.info("PDF created!")
    
    if poster:
        poster_file = Path("poster.jpg")
        with open(poster_file, "wb+") as f:
            f.write(processed[0])
        return poster_file.absolute()