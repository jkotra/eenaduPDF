import os
import logging


logger = logging.getLogger(__name__)

def freeup_space(y, needed_space: int):
    info = y.get_disk_info()
    
    total = info.total_space
    used = info.used_space

    logger.info(f"INITIAL: total = {total} used = {used} free = {total-used}")

    files = sorted(list(y.get_files()), key=lambda x: x.created, reverse=True)

    while((total - used) < needed_space and len(files) >= 1):
        y.remove(files[-1].path, permanently=True)
        
        # update
        info = y.get_disk_info()
        total = info.total_space
        used = info.used_space
        files = sorted(list(y.get_files()), key=lambda x: x.created, reverse=True)
        logger.info(f"total = {total} used = {used} free = {total-used}")


def upload(y, file: str) -> str:
    size = os.stat(file).st_size
    freeup_space(y, size)
    y.upload(file, dst_path=file, overwrite=True)
    y.publish(file)
    meta = y.get_meta(file)
    logger.info("Download link: %s", meta.public_url)
    return meta.public_url
